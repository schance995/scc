# scc

Read [shellcheck](https://github.com/koalaman/shellcheck) entries in the terminal.

## Installation

```
git clone https://github.com/schance995/sc.git
cd scc
ln -s scc {somewhere in your path}
```

## Usage

To read entry 2002 (useless use of cat):

`scc 2022`

`scc` will update its cache by running `git pull` if it is over 1 week old.

TIP: install `mdcat` to improve the Markdown terminal experience.

## License

ISC
